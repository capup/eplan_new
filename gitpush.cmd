@echo off
color 0E

SET user_name=Volodymyr L
SET user_email=60s24h@gmail.com
SET https_git=https://gitlab.com/capup/eplan_new.git

echo:Is is correct?
echo:user: %user_name%
echo:email: %user_email%
echo:https: %https_git%
echo:...(Press ``Enter``)
pause > nul

git --version
git config --global user.name %user_name%
git config --global user.email %user_email%
git init
git remote add origin %https_git%
git add .
git status
git commit -m "%date%_%time%"
SET /P branch = "Branch name is ('master'): "
IF "%branch%"=="" (
    SET branch=master
)
git push --set-upstream origin %branch%
start "" "%https_git%"
echo:... (git push is finised! Press ``Enter``)
pause > nul

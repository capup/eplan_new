from django.urls import path
from .views import homepage, table


urlpatterns = [
    path("test/", homepage, name="homepage"),
    path("table/", table, name="table"),
    #path("post/<int:pk>-<str:slug>/", post_details, name="post-details"),
    #path("cats/<str:cat>", cat_page, name="cat_page"),
]

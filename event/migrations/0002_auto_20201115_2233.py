# Generated by Django 3.1.2 on 2020-11-15 20:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='awards',
            field=models.ManyToManyField(to='event.AwardsList', verbose_name='Нагороди'),
        ),
    ]

# Generated by Django 3.1.2 on 2020-11-15 22:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0008_auto_20201116_0050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usermeta',
            name='organization_type',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='event.organizationtypelist', verbose_name='Тип організації'),
        ),
    ]

# Generated by Django 3.1.2 on 2020-11-15 22:32

import django.contrib.auth.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0004_auto_20201116_0027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='audience',
            field=models.ManyToManyField(to='event.AudienceList', verbose_name='Цільова аудиторія'),
        ),
        migrations.AlterField(
            model_name='event',
            name='department',
            field=models.CharField(default=django.contrib.auth.models.User, editable=False, max_length=64, verbose_name='Відділ'),
        ),
        migrations.AlterField(
            model_name='event',
            name='image1',
            field=models.ImageField(blank=True, upload_to='uploads/%Y/%m/%d/', verbose_name='Загальне фото'),
        ),
        migrations.AlterField(
            model_name='event',
            name='image2',
            field=models.ImageField(blank=True, upload_to='uploads/%Y/%m/%d/', verbose_name='Фото заходу'),
        ),
        migrations.AlterField(
            model_name='event',
            name='image3',
            field=models.ImageField(blank=True, upload_to='uploads/%Y/%m/%d/', verbose_name='Фото нагородження'),
        ),
        migrations.AlterField(
            model_name='event',
            name='link2',
            field=models.CharField(blank=True, default='', max_length=512, verbose_name='Посилання #2'),
        ),
        migrations.AlterField(
            model_name='event',
            name='link3',
            field=models.CharField(blank=True, default='', max_length=512, verbose_name='Посилання #3'),
        ),
    ]

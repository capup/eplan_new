# Generated by Django 3.1.2 on 2020-11-15 22:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0006_auto_20201116_0033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='comments',
            field=models.TextField(blank=True, max_length=512, verbose_name='Коментар'),
        ),
    ]

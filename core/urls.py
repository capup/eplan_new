from django.urls import path
from .views import test1, test2


urlpatterns = [
    path("test1/", test1, name="test1"),
    path("test2/", test2, name="test2"),
    #path("post/<int:pk>-<str:slug>/", post_details, name="post-details"),
    #path("cats/<str:cat>", cat_page, name="cat_page"),
]
